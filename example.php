<?php
require('formy.php');

$form = new Formy;
$form->create('example');

/**
 * ===============================
 * ===============================
 *         All Elements
 * ===============================
 * ===============================
 */

$form->fieldset('All Elements');

$form->input(array(
	'type' => 'text',
	'name' => 'v_text',
	'label' => 'Text',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'password',
	'name' => 'v_pass',
	'label' => 'Password',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'textarea',
	'name' => 'v_textarea',
	'label' => 'Textarea',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'checkbox',
	'name' => 'v_check',
	'label' => 'Checkbox',
	'wrap' => '<div>',
	'value' => 'this is a check'
));

$form->input(array(
	'type' => 'file',
	'name' => 'v_file',
	'label' => 'File',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'hidden',
	'name' => 'v_hidden',
	'label' => 'Hidden',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'radio',
	'name' => 'v_radio',
	'label' => 'Radio',
	'options' => array(
		'one',
		'two'
	),
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'select',
	'name' => 'v_select',
	'label' => 'Select',
	'wrap' => '<div>',
	'options' => array(
		'one',
		'two',
		'three'
	)
));

$form->input(array(
	'type' => 'reset',
	'name' => 'v_reset',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'submit',
	'name' => 'v_submit',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'button_submit',
	'name' => 'v_bs',
	'value' => 'Button Submit',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'button_reset',
	'name' => 'v_br',
	'value' => 'Button Reset',
	'wrap' => '<div>'
));

$form->input(array(
	'type' => 'button_button',
	'name' => 'b_bb',
	'value' => 'Button',
	'wrap' => '<div>'
));


/**
 * ===============================
 * ===============================
 *        Display Options
 * ===============================
 * ===============================
 */

$form->fieldset(array(
	'legend' => 'Display Options',
	'after' => '<em><u>These fields are tabindexed</u></em>'
));

// Text input with a placeholder and label after the input
$form->input(array(
	'type' => 'text',
	'name' => 'vd_latext',
	'label' => array(
		'label' => '&hellip;and Label after',
		'after' => true
	),
	'placeholder' => 'Placeholders',
	'wrap' => '<div>',
	'tabindex' => 1
));

// Place markup directly before the input
$form->input(array(
	'type' => 'text',
	'name' => 'vd_betext',
	'label' => 'LABEL',
	'wrap' => '<div>',
	'before' => '\'<code>before</code>\' positioned before the input <em>(default)</em>',
	'tabindex' => 2
));

// Place markup directly before the label
// (if label is placed before the input)
$form->input(array(
	'type' => 'text',
	'name' => 'vd_beltext',
	'label' => 'LABEL',
	'title' => 'This will never be tabbable (tabindex="-1")',
	'before' => array(
		'value' => '\'<code>before</code>\' positioned before the label',
		'before_label' => true
	),
	'tabindex' => '-1'
));

// Add classes
$form->input(array(
	'type' => 'text',
	'name' => 'vd_cltext',
	'label' => 'Classes <em>(field and wrap)</em>',
	'class' => 'hello world',
	'wrap' => '<div class="field">',
	'tabindex' => 4
));

// Change input size
$form->input(array(
	'type' => 'text',
	'name' => 'vd_stext',
	'label' => 'Sized input',
	'size' => 60,
	'wrap' => '<div>',
	'tabindex' => 3
));

/**
 * ===============================
 * ===============================
 *        Element Options
 * ===============================
 * ===============================
 */

$form->fieldset('Elements With Options');

// Read only text
$form->input(array(
	'type' => 'text',
	'name' => 'vf_rtext',
	'label' => 'Read-only text',
	'readonly' => true,
	'value' => 'Can\'t delete me',
	'wrap' => '<div>'
));

// Maximum input length
$form->input(array(
	'type' => 'text',
	'name' => 'vf_maxtext',
	'label' => 'Maximum length text',
	'maxlength' => 5,
	'placeholder' => 'Type up to 5 characters',
	'wrap' => '<div>'
));

// Disabled field
$form->input(array(
	'type' => 'password',
	'name' => 'vf_pass',
	'label' => 'Disabled Password',
	'wrap' => '<div>',
	'disabled' => true
));

// Cols and Rows for textareas
$form->input(array(
	'type' => 'textarea',
	'name' => 'vf_textarea',
	'label' => 'Textarea cols and rows',
	'wrap' => '<div>',
	'cols' => 30,
	'rows' => 10
));

// Checkbox pre-checked
$form->input(array(
	'type' => 'checkbox',
	'name' => 'vf_check',
	'label' => 'Checked checkbox',
	'wrap' => '<div>',
	'value' => 'this is a check',
	'checked' => true
));

// Radio option pre-selected
$form->input(array(
	'type' => 'radio',
	'name' => 'vf_radio',
	'label' => 'Checked radio',
	'options' => array(
		'one',
		'two',
		array(
			'label' => 'three',
			'checked' => true
		)
	),
	'wrap' => '<div>'
));

// Add values to select fields, select and disable specific ones
$form->input(array(
	'type' => 'select',
	'name' => 'vf_select',
	'label' => 'Select with specified values and disabled options',
	'wrap' => '<div>',
	'options' => array(
		'Nothing' => array(
			'label' => 'zero',
			'disabled' => true
		),
		'This is One' => array(
			'label' => 'one',
			'selected' => true
		),
		'Two for Luck' => array(
			'label' => 'two'
		),
		'The Magic Number' => array(
			'label' => 'one'
		),
	)
));

/**
 * ===============================
 * ===============================
 *          Validation
 * ===============================
 * ===============================
 */

$form->fieldset('Validation');

// Required field
// To hide asterisks, add 'show_required' => false to $form->create options
$form->input(array(
	'type' => 'text',
	'name' => 'vv_rtext',
	'label' => 'Required',
	'wrap' => '<div>',
	'after' => '<br /><em>Asterisks can be disabled using the <code>show_required</code> attribute on <code>$form->create()</code>.</em>',
	'required' => true
));

// Set allowed input types
$form->input(array(
	'type' => 'text',
	'name' => 'vv_atext',
	'label' => 'Alphanumeric',
	'wrap' => '<div>',
	'accepts' => 'text_alphanumeric',
	'after' => '<em>letters, numbers and underscores</em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_ahtext',
	'label' => 'Alphanumeric <em>with -</em>',
	'wrap' => '<div>',
	'accepts' => 'text_alphanumeric_allow_-',
	'after' => '<em>letters, numbers, underscores and hyphens</em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_altext',
	'label' => 'Alpha',
	'wrap' => '<div>',
	'accepts' => 'text_alpha',
	'after' => '<em>letters only</em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_ntext',
	'label' => 'Numbers',
	'wrap' => '<div>',
	'accepts' => 'number',
	'after' => '<em>numbers only</em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_amtext',
	'label' => 'Alpha mixed',
	'wrap' => '<div>',
	'accepts' => 'text_alpha_allow_spacessymbols',
	'after' => '<em>letters, symbols and spaces</em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_url',
	'label' => 'URL',
	'wrap' => '<div>',
	'accepts' => 'url',
	'after' => '<em>Full URL e.g. <code>http://www.google.com</code></em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_rurl',
	'label' => 'Relative URL',
	'wrap' => '<div>',
	'accepts' => 'url_relative',
	'after' => '<em>Url or Path e.g. <code>assets/img/hello.png</code></em>',
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_email',
	'label' => 'Email',
	'wrap' => '<div>',
	'accepts' => 'email'
));

$form->input(array(
	'type' => 'text',
	'name' => 'vv_date',
	'label' => 'Date',
	'wrap' => '<div>',
	'accepts' => 'adate',
	'after' => '<em>e.g. <code>25th December, 25-12-'.date('Y').', 12/25/'.date('Y').'</code>. See <a href="http://php.net/manual/en/function.strtotime.php" target="_blank">spec</a></em>.',
));

// Set a custom error message
$form->input(array(
	'type' => 'text',
	'name' => 'vv_error',
	'label' => 'Error',
	'wrap' => '<div>',
	'accepts' => 'text_symbols',
	'required' => true,
	'after' => '<em>Custom error message</em>',
	'error_msg' => 'This has a custom message'
));

$form->input(array(
	'type' => 'file',
	'name' => 'vv_rfile[]',
	'label' => 'Required File',
	'wrap' => '<div>',
	'required' => true
));

// End a fieldset using FALSE
$form->fieldset(false);

$form->input(array(
	'type' => 'submit',
	'name' => 'form_submit',
	'wrap' => '<div>'
));


?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
		<style>
			label {
				font-weight: bold;
			}
			.hello {
				border: 1px solid #999;
				padding: 3px;
			}

			.world {
				border-radius: 5px;
			}

			.field label {
				display: block;
			}

			.error {
				border: 1px solid #ff0000;
			}

			.code {
				border: 1px solid #666;
				background-color: #DDD;
				padding: 20px;
				margin-bottom: 10px;
			}

			.code h3 {
				margin: 0;
			}
		</style>
		<div id="main">
			<h1>Formy Example</h1>
			<?php if ($_POST): ?>
			<?php echo "<div class=\"code\"><h3>POST data</h3>" . print_r($_POST, false) . '</div>'; ?>
			<?php echo "<div class=\"code\"><h3>FILES data</h3>" . print_r($_FILES, false) . '</div>'; ?>
			<?php echo "<div class=\"code\"><h3>Parsed data</h3>" . print_r($form->post(), false) . '</div>'; ?>
			<?php
			if ($errors = $form->errors()) {
				echo "<div class=\"code\"><h3>Validation Errors</h3>" . print_r($errors, false) . '</div>';
			}
			?>
			<?php endif; ?>
			<?php $form->render(); ?>

			<?php
			/**
			    Instructions
			    =================
				$form->render( [ $id = null , $echo = true ] ) - Echoes the form. Either a specific form or the
														 		 most recently altered. Can also be returned.

				$form->post( [ $id = null ] ) - Returns the parsed form data

				$form->errors( [ $id = null ] ) - Returns any validation errors
			*/

			?>
		</div>
		<!-- //End: Main -->

</body>
</html>

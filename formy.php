<?php
/**
* Core
*/
class FormCore
{
	private $id;
	private $forms;

	function set_id($id) {
		$this->id = $id;
	}

	function new_form($args) {
		$this->forms[$this->id]['form'] = $args;
		$this->forms[$this->id]['inputs'] = array();
	}

	function add_field($fields, $id = null) {
		$id = $id ? $id : $this->id;

		$this->forms[$id]['inputs'][] = $fields;
	}

	function update_form($args, $id = null) {
		$id = $id ? $id : $this->id;

		$_args = $this->forms[$id]['form'];
		$this->forms[$id]['form'] = array_merge($_args, $args);
	}

	function get_form($id = null) {
		if ($id && array_key_exists($id, $this->forms)) {
			return $this->forms[$id];
		} elseif ($id) {
			return false;
		}

		return $this->forms;
	}

	function get_form_attr($attr, $id = null) {
		$id = $id ? $id : $this->id;

		$_args = $this->forms[$id]['form'];
		if (isset($_args[$attr])) {
			return $_args[$attr];
		}

		return false;
	}

	function count_fields($type = null, $id = null) {
		$id = $id ? $id : $this->id;

		if ($type) {
			$count = 0;

			foreach ($this->forms[$id]['inputs'] as $field) {
				if ($field['type'] === $type) $count++;
			}

			return $count;
		}

		return count($this->forms[$id]['inputs']);
	}

	function __destruct() {
		// a::pr($this->forms);
	}
}


/**
* Formy
*/
class Formy extends FormCore
{

	var $post_data = array();

	var $form_args = array(
		'id' => '',
		'name' => '',
		'method' => 'POST',
		'action' => '',
		'prepend' => '',
		'append' => '',
		'show_required' => false,
		'class' => '',
		'autocomplete' => '',
		'novalidate' => '',
		'enctype' => null
	);

	protected $tags = array(
		'checkbox' => array(
			'checked' => array(
				'accepts' => 'bool'
			)
		),
		'file' => array(
			'accept' => array(
				'accepts' => 'mime'
			),
			'multiple' => array(
				'accepts' => 'bool'
			)
		),
		'hidden' => '',
		'password' => array(
			'maxlength' => array(
				'accepts' => 'number'
			),
			'readonly' => array(
				'accepts' => 'bool'
			),
			'placeholder' => array(
				'accepts' => 'text'
			)
		),
		'radio' => array(
			'checked' => array(
				'accepts' => 'bool'
			),
			'options' => array(
				'accepts' => 'anarray'
			)
		),
		'reset' => '',
		'submit' => '',
		'image' => array(
			'alt' => array(
				'accepts' => 'text'
			),
			'src' => array(
				'accepts' => 'url_relative'
			)
		),
		'text' => array(
			'maxlength' => array(
				'accepts' => 'number'
			),
			'readonly' => array(
				'accepts' => 'bool'
			),
			'placeholder' => array(
				'accepts' => 'text'
			),
			'autocapitalize' => ''
		),
		'textarea' => array(
			'placeholder' => array(
				'accepts' => 'text'
			),
			'readonly' => array(
				'accepts' => 'bool'
			),
			'rows' => array(
				'accepts' => 'number'
			),
			'cols' => array(
				'accepts' => 'number'
			)
		),
		'select' => array(
			'options' => array(
				'accepts' => 'anarray'
			),
			'selected' => array(
				'accepts' => 'bool'
			)
		),
		'button_submit' => '',
		'button_reset' => '',
		'button_button' => '',
		'fieldset' => array(
			'legend' => array(
				'accepts' => 'text'
			),
			'end' => array(
				'accepts' => 'bool'
			)
		)
	);

	protected $common = array(
		'disabled' => array(
			'accepts' => 'bool'
		),
		'name' => array(
			'accepts' => 'text_alphanumeric_allow_-\[\]'
		),
		'size' => '',
		'value' => '',
		'tabindex' => array(
			'accepts' => 'text_numeric_allow_-'
		),
		'autofocus' => array(
			'accepts' => 'bool'
		),
		'label' => '',
		'class' => '',
		'id' => '',
		'title' => '',

		'before' => '',
		'after' => '',
		'wrap' => '',

		'accepts' => '',
		'required' => array(
			'accepts' => 'bool'
		),
		'callback' => '',
		'error_msg' => ''
	);

	function __construct() {
		if ($_POST && isset($_POST['formyid'])) {
			$this->setpost($_POST);
		}
	}

	function setpost($post) {
		if (!isset($post['formyid'])) return false;

		$post = array_map('trim', $post);

		if (get_magic_quotes_gpc()) {
			$post = array_map('stripslashes', $post);
		}

		$id = $post['formyid'];
		unset($post['formyid']);
		$this->post_data[$id] = $post;
	}

	function create($args = null) {
		if ($args && !is_array($args)) {
			$args = array('id' => $args);
		}

		if (!isset($args['id']) || empty($args['id'])) {
			if (isset($args['name']) && !empty($args['name'])) {
				$args['id'] = $args['name'];
			} else {
				$args['id'] = str::random(5);
			}
		}

		$this->id = $args['id'];

		$args = array_merge($this->form_args, $args);

		$args['method'] = strtoupper($args['method']);

		if ($args['method'] != 'POST' && $args['method'] != 'GET') {
			$args['method'] = 'POST';
		}

		parent::set_id($this->id);
		parent::new_form($args);
	}

	function input($args, $id = null) {
		$id = ($id) ? $id : $this->id;

		if (!parent::get_form($id)) return false;

		$this->temp_inputs[$id][] = $args;
	}

	function fieldset($args = null, $id = null) {
		$id = ($id) ? $id : $this->id;

		if (!parent::get_form($id)) return false;

		$_args = array(
			'type' => 'fieldset'
		);

		if ($args === false) {
			$_args['end'] = true;
		} elseif (is_string($args)) {
			$args = array(
				'legend' => $args
			);
			$_args = array_merge($_args, $args);
		} elseif (is_array($args)) {
			$_args = array_merge($_args, $args);
		}

		$this->temp_inputs[$id][] = $_args;
	}

	function do_temp() {
		if (!isset($this->temp_inputs[$this->id])) return false;

		foreach ($this->temp_inputs[$this->id] as $input) {
			$this->do_input($input);
		}

		unset($this->temp_inputs[$this->id]);
	}

	function do_input($args) {

		if (!isset($this->id)) return false;

		if (!is_array($args)) {
			$inputs = array(
				'type' => $args
			);
		} elseif(!isset($args['type'])) {
			return false;
		} elseif(@$args['name'] == 'formyid') {
			return false;
		} else {
			$inputs = $args;
		}

		$type = $inputs['type'];

		if (array_key_exists($type, $this->tags)) {
			$allowed = $this->common;

			if (is_array($this->tags[$type])) {
				$allowed = array_merge($this->common, $this->tags[$type]);
			}

			$inputs = self::parse_tags($inputs, $allowed);

			if (!isset($inputs['name'])) {
				$count = parent::count_fields($type) + 1;

				$inputs['name'] = $type . '-' . $count;
			} else {
				$inputs['name'] = $inputs['name'];
			}

			// Set the form enctype if file field exists and no enctype is set
			if ($type == 'file' && !parent::get_form_attr('enctype')) {
				parent::update_form(array('enctype' => "multipart/form-data"));
			}

			// if (($type == 'radio' || $type == 'select') && isset($inputs['options'])) {
			// 	foreach ($inputs['options'] as $opkey => $option) {
			// 		if (!is_array($option)) {
			// 			$inputs['options'][$opkey] = array(
			// 				'label' => $option
			// 			);
			// 		}
			// 	}
			// }

			if (isset($this->post_data[$this->id]) && $type !== 'file') {
				$post_data = $this->post_data[$this->id];

				if (!preg_match('/^button/i', $type) && array_key_exists($inputs['name'], $post_data)) {
					if ($type == 'checkbox') {
						if ((!isset($inputs['value']) && $post_data[$inputs['name']] == 'on') || stripslashes($post_data[$inputs['name']]) == $inputs['value']) {
							$inputs['checked'] = true;
						}
					} else {
						$inputs['value'] = $post_data[$inputs['name']];
					}

					// Validate input
					if (!empty($inputs['value']) && isset($inputs['accepts']) && !$valid = TagValidation::v($inputs['value'], $inputs['accepts'])) {
						$inputs['error'] = 'invalid';
					}

					if (empty($inputs['value']) && @$inputs['required'] === true) {
						$inputs['error'] = 'required';
					}
				} elseif (isset($inputs['required']) && $inputs['required'] === true) {
					$inputs['error'] = 'required';
				}
			}

			// Remove array names
			$clean_name = preg_replace('/\[.*\]/', '', $inputs['name']);

			if ($type === 'file' && isset($_FILES[$clean_name])) {
				$file = $_FILES[$clean_name];

				if (is_array($file['error'])) {
					$error = reset($file['error']); // Get the first error if file is multiple
				} else {
					$error = $file['error'];
				}

				if (isset($inputs['required']) && $error == 4) {
					$inputs['error'] = 'required';
				}

				if ($error == 1 || $error == 2) {
					$inputs['error'] = 'too large';
				}
			}

			if (isset($inputs['error']) && isset($inputs['error_msg'])) {
				$inputs['error'] = array(
					'error' => $inputs['error'],
					'message' => $inputs['error_msg']
				);
			}

			// if (($type == 'radio' || $type == 'checkbox') && isset($inputs['value']) && $inputs['value'] == 'on') {
			// 	$inputs['checked'] = true;
			// 	unset($inputs['value']);
			// }

			// if (isset($inputs['class']) && !is_array($inputs['class'])) {
			// 	$inputs['class'] = explode(' ', $inputs['class']);
			// }

			if (isset($inputs['callback']) && !isset($inputs['error']) && !empty($inputs['value']) && isset($post_data)) {
				$cb = explode('::', $inputs['callback']);

				if (count($cb) >= 2 && (method_exists($cb[0], $cb[1]) || function_exists($cb[0]))) {
					// ob_start();
					$callback = call_user_func($cb, $inputs);
					// ob_end_clean();

					$inputs = array_merge($inputs, self::parse_tags($callback, $allowed));

					if (isset($callback['error'])) {
						$inputs['error'] = $callback['error'];
					}

					$inputs['callback'] = $callback;
				}
			}

			parent::add_field($inputs);
			return $inputs;
		}
	}

	function parse_tags($inputs, $allowed) {

		foreach ($inputs as $k => $v) {
			if (!array_key_exists($k, $allowed) && $k != 'type') {
				unset($inputs[$k]);
				continue;
			}

			if (is_array($v) && empty($v)) {
				unset($inputs[$k]);
				continue;
			} elseif (!is_array($v) && trim($v) === "") {
				unset($inputs[$k]);
				continue;
			}

			if (isset($allowed[$k]['accepts']) && !TagValidation::v($v, $allowed[$k]['accepts'])) {
				unset($inputs[$k]);
				continue;
			}
		}

		return $inputs;
	}

	function render($id = null, $echo = true) {
		$id = ($id) ? $id : $this->id;

		if (!parent::get_form($id)) return false;

		$this->id = $id;
		parent::set_id($id);
		$this->do_temp();

		if ($form = parent::get_form($id)) {
			$renderer = new FormRender(array($form));
			return $renderer->render($echo);
		}

		return false;
	}

	function post($id = null) {
		$id = ($id) ? $id : $this->id;

		if (!array_key_exists($id, $this->post_data)) return false;

		$this->id = $id;
		parent::set_id($id);

		$this->do_temp();

		$form = parent::get_form($id);

		$return = array();

		foreach ($form['inputs'] as $input) {
			if ($input['type'] == 'fieldset') continue;

			$value = '';

			if ($input['type'] == 'checkbox') {
				if (isset($input['checked'])) {
					$return[$input['name']] = $input['value'];
				}
			} else if (isset($input['value'])) {
				$return[$input['name']] = $input['value'];
			}
		}

		return $return;
	}

	function errors($id = null) {
		$id = ($id) ? $id : $this->id;

		if ($form = parent::get_form($id)) {
			$errors = array();

			foreach($form['inputs'] as $i) {
				if (isset($i['error'])) {
					$error = array(
						'field' => $i['name'],
						'error' => $i['error']
					);

					if (isset($i['label'])) {
						if (is_array($i['label'])) {
							$error['label'] = $i['label']['label'];
						} else {
							$error['label'] = $i['label'];
						}
					}

					if (isset($i['accepts']) && $i['error'] == 'invalid') {
						$error['accepts'] = $i['accepts'];
					}

					$errors[] = $error;
				}
			}

			return $errors;
		}

		return false;
	}

	static function strip_returns($input) {
		$return = array();

		$return['value'] = preg_replace('/\s+/', ' ', $input['value']);

		return $return;
	}

	static function strip_tags($input) {
		$return = array();

		$return['value'] = strip_tags($input['value']);

		return $return;
	}
}


class TagValidation extends FormCore
{
	function v($data, $method) {
		$validator = $method;

		if (preg_match('/^([a-z]+)_(.*)/', $method, $matches)) {
			$validator = $matches[1];
			return @self::$validator($data, $matches[2]);
		}

		return @self::$validator($data);
	}

	static function bool($data) {
		if (is_bool($data)) {
			return true;
		}

		return false;
	}

	static function anarray($data) {
		if (is_array($data)) {
			return true;
		}

		return false;
	}

	static function url($data, $args = null) {
		if ($args && $args == 'relative') {
			$data = str_replace('http://', '', $data);
			$data = 'http://' . $data;
		}

		if (filter_var($data, FILTER_VALIDATE_URL)) {
			$host = parse_url($data, PHP_URL_HOST);
			if (!preg_match("/\.[\w]{2,}$/", $host)) return false;

			return true;
		}

		return false;
	}

	static function email($data) {
		if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
			if (!preg_match("/\.[\w]{2,}$/", $data)) return false;

			return true;
		}

		return false;
	}

	static function adate($data) {
		if (strtotime($data)) {
			return true;
		}
		return false;
	}

	static function number($data) {
		if (preg_match('/^[\d]+$/', $data)) {
			return true;
		}
		return false;
	}

	static function text($data, $args = null) {
		$allowed = array(
			'symbols' => '\W',
			'spaces' => '\s',
			'numeric' => '\d',
			'alpha' => 'a-zA-Z',
			'alphanumeric' => '\w'
		);

		if ($args) {
			if (strstr($args, '_allow_')) {
				list($args, $allow) = explode('_allow_', $args);

				foreach ($allowed as $short => $regex) {
					if (strstr(strtolower($allow), $short)) {
						$allow = str_ireplace($short, $regex, $allow);
					}
				}

				if (preg_match_all('#(\\\?.)#', $allow, $matches)) {
					$allow = $matches[0];
				}
			}

			if ($flags = strstr($args, 'no_')) {
				$flags = explode('_', $flags);

				foreach ($flags as $flag) {
					unset($allowed[$flag]);
				}

			} else {
				$flags = explode('_', $args);

				$static = array();

				foreach ($flags as $flag) {
					if (isset($allowed[$flag])) {
						$static[] = $allowed[$flag];
					}
				}

				$allowed = $static;
			}
		}

		$flags = implode('', $allowed);

		if (isset($allow)) {
			// foreach ($allow as $key => $flag) {
			// 	// if (in_array($tag, $reserved)) {
			// 		$allow[$key] = '\\' . $flag;
			// 	// }
			// }

			$flags .= implode('', $allow);
		}

		if(preg_match('/^['.$flags.']+$/', $data)) {
			return true;
		}

		return false;
	}
}


/**
*
*/
class FormRender extends FormCore
{

	private $form;
	static $tab = 1;

	function __construct($form) {
		$this->form = $form;
	}

	function tab($count = null) {
		$count = $count !== null ? $count : self::$tab;
		return str_repeat("\t", $count);
	}

	function render($echo = true) {
		foreach ($this->form as $f) {

			$attr = $f['form'];
			unset($attr['prepend'], $attr['append'], $attr['show_required']);

			$attributes = '';

			foreach ($attr as $att => $val) {
				if (!empty($val)) {
					$attributes .= " $att=\"$val\"";
				}
			}

			$fields = '';
			$output = "\n" . '<form' . $attributes . '>' . "\n";

			$has_required = false;

			$fieldsets = 0;

			foreach ($f['inputs'] as $field) {
				$before = '';
				$after = '';
				$wrap_start = '';
				$wrap_end = '';
				self::$tab = $fieldsets >= 1 ? 2 : 1;

				if (isset($field['wrap'])) {
					if (preg_match('/<([a-zA-Z]+)/i', $field['wrap'], $matches)) {
						$wrap_start = self::tab() . $field['wrap'] . "\n";
						$wrap_end = self::tab() . '</'.$matches[1].'>';
						self::$tab++;
					}
				}

				if (isset($field['before']) && is_array($field['before']) && @$field['before']['before_label'] === true) {
					if (is_array($field['before'])) {
						$field['before'] = @$field['before']['value'];
					}

					$before = self::tab() . $field['before'] . "\n";
					unset($field['before']);
				}

				if (isset($field['after']) && (!is_array($field['after']) || @$field['after']['after_label'] === false)) {
					if (is_array($field['after'])) {
						$field['after'] = @$field['after']['value'];
					}

					$after = self::tab() . $field['after'] . "\n";
					unset($field['after']);
				}

				unset($field['wrap'], $field['error_msg']);

				// Labels
				if (isset($field['label']) && !empty($field['label'])) {
					$l = $field['label'];
					$attr = '';
					$position = 'before';

					if (!is_array($l)) {
						$label = $l;
					} else {
						if (!isset($l['label'])) {
							continue;
						}

						$label = $l['label'];
						unset($l['label']);

						$position = isset($l['after']) && $l['after'] === true ? 'after' : 'before';
						unset($l['after']);

						foreach ($l as $tag => $val) {
							if (is_bool($val)) {
								if ($val) {
									$attr .= ' ' . $tag;
								}
							} else {
								$attr .= ' ' . $tag . '="' . $val . '"';
							}
						}
						unset($tag, $val);
					}

					$for = isset($field['id']) ? ' for="' . $field['id'] . '"' : '';

					$required = @$field['required'] === true ? '<abbr title="Required">*</abbr>' : null;
					if ($required) $has_required = true;

					if ($position == 'before') {
						$before .= self::tab() . '<label' . $for . $attr . '>' . $label . $required . '</label>' . "\n";
					} else {
						$after .= self::tab() . '<label' . $for . $attr . '>' . $label . $required . '</label>' . "\n";
					}

					unset($field['label'], $l, $label);
				}
				// --Labels

				if (isset($field['before'])) {
					if (is_array($field['before'])) {
						$field['before'] = $field['before']['value'];
					}

					$before .= self::tab() . $field['before'] . "\n";
					unset($field['before']);
				}

				if (isset($field['after'])) {
					$after .= self::tab() . $field['after']['value'] . "\n";
					unset($field['after']);
				}

				// Input
				$attr = array();

				// if (isset($field['name']) && array_key_exists($field['name'], $this->post_data)) {
				// 	$field['value'] = $this->post_data[$field['name']];
				// }

				unset($field['accepts'], $field['required'], $field['callback']);

				if (isset($field['error'])) {
					if (isset($field['class'])) {
						$field['class'] .= ' error';
						trim($field['class']);
					} else {
						$field['class'] = 'error';
					}
					unset($field['error']);
				}

				// if (isset($field['class'])) {
				// 	$field['class'] = implode(' ', $field['class']);
				// }

				foreach ($field as $tag => $val) {
					if (is_bool($val)) {
						if ($val) {
							$attr[$tag] = $tag;
						}
					} elseif (!is_array($val) && !(is_string($val) && empty($val))) {
						if ($tag == 'placeholder' || $tag == 'label' || $field['type'] == 'submit' || $field['type'] == 'button') {
							$attr[$tag] = "$tag=\"$val\"";
						} else {
							$attr[$tag] = "$tag=\"".htmlentities($val, ENT_QUOTES)."\"";
						}
					}
				}

				if ($field['type'] == 'textarea' || $field['type'] == 'select' || $field['type'] == 'fieldset') {
					unset($attr['value'], $attr['type'], $attr['options']);
				}

				if ($field['type'] == 'fieldset') {
					unset($attr['name'], $attr['legend']);
				}

				if ($field['type'] == 'radio') {
					unset($attr['value']);
				}

				if ($field['type'] == 'checkbox') {
					unset($attr['options']);
				}

				if (preg_match('/^button_(.*)/i',$field['type'], $match)) {
					unset($attr['value'], $attr['type']);
					$field['type'] = 'button';

					if (isset($match[1])) {
						$attr[] = 'type="' . $match[1] . '"';
					}
				}
				unset($match);

				$attr = implode(' ', $attr);

				$fields .= $wrap_start;
				$fields .= $before;

				switch ($field['type']) {
					case 'textarea':
						$fields .= self::tab() . '<textarea ' . $attr . '>'.@$field['value'].'</textarea>' . "\n";
						break;
					case 'select':
						if (isset($field['options'])) {
							$fields .= self::tab() . '<select ' . $attr . '>' . "\n";
							foreach ($field['options'] as $key => $option) {
								$selected = $disabled = '';


								if ((isset($field['value']) && (string)$key == $field['value']) || @$option['selected'] === true) {
									$selected = ' selected';
								}

								if (is_array($option)) {
									if (isset($option['disabled'])) {
										$disabled = ' disabled';
									}

									$option = $option['label'];
								}

								$fields .= self::tab() . "\t" . '<option value="'.$key.'"'.$selected.''.$disabled.'>'.$option.'</option>' . "\n";
							}
							$fields .= self::tab() . '</select>' . "\n";
						}
						break;
					case 'radio':
						if (isset($field['options'])) {
							foreach ($field['options'] as $key => $option) {
								$selected = $disabled = '';

								if (is_array($option)) {
									if (isset($option['disabled'])) {
										$disabled = ' disabled';
									}

									if (isset($option['checked'])) {
										$selected = ' checked';
									}

									$option = $option['label'];
								}

								if (isset($field['value']) && (string)$key == $field['value']) {
									$selected = ' checked';
								} elseif (isset($field['value'])) {
									$selected = '';
								}

								$fields .= self::tab() . "<div>\n";
								$fields .= self::tab() . "\t" . '<input '.$attr.' value="'.$key.'"'.$selected.''.$disabled.' />' . $option . "\n";
								$fields .= self::tab() . "</div>\n";
							}
						}
						break;
					case 'button':
						$fields .= self::tab() . '<button ' . $attr . '>'.@$field['value'].'</button>' . "\n";
						break;
					case 'fieldset':
						if ($fieldsets >= 1) {
							self::$tab--;
							$fields .= self::tab() . "</fieldset>\n";
							$fieldsets = 0;
						}

						if (!isset($field['end'])) {
							$fieldsets++;
							$attr = empty($attr) ? null : ' ' . $attr;
							$fields .= self::tab() . "<fieldset$attr>\n";
							if (isset($field['legend'])) {
								$fields .= self::tab() . "\t" . '<legend>'.$field['legend'].'</legend>';
							}
						}
						break;
					default:
						$fields .= self::tab() . '<input ' . $attr . ' />' . "\n";
				}

				$fields .= $after;
				$fields .= $wrap_end . "\n";
			} // end foreach

			self::$tab = 1;

			if (!empty($f['form']['prepend'])) {
				$output .= self::tab() . $f['form']['prepend'] . "\n";
			}

			if ($has_required && $f['form']['show_required'] === true) $output .= '<em>* required field</em>';

			$output .= $fields;

			if ($fieldsets >= 1) {
				$output .= self::tab() . "</fieldset>\n";
			}

			$output .= self::tab() . '<input type="hidden" name="formyid" value="'.$f['form']['id'].'" />' . "\n";

			if (!empty($f['form']['append'])) {
				$output .= self::tab() . $f['form']['append'] . "\n";
			}

			$output .= "</form>\n";


			if ($echo) {
				echo $output;
			} else {
				return $output;
			}
		}
			// return $output;
	}
}
